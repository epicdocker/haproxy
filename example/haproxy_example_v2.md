# HAProxy example

<!-- vscode-markdown-toc -->
* 1. [Prepare](#Prepare)
* 2. [Example](#Example)
* 3. [Statistics](#Statistics)
* 4. [Reload configuration](#Reloadconfiguration)
* 5. [Cleanup](#Cleanup)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

##  1. <a name='Prepare'></a>Prepare

Download Docker compose and HAProxy configuration file

```bash
mkdir -p /tmp/haproxy_example 
curl -o /tmp/haproxy_example/haproxy.cfg https://gitlab.com/epicdocker/haproxy/raw/master/example/haproxy.cfg
curl -o /tmp/haproxy_example/haproxy_example_v2.yml https://gitlab.com/epicdocker/haproxy/raw/master/example/haproxy_example_v2.yml
```

##  2. <a name='Example'></a>Example

Now we start the Docker compose file, which starts three web servers and one HAProxy. The web server delivers different contents so that we can clearly see the distribution of the requests.

```bash
docker-compose -f /tmp/haproxy_example/haproxy_example_v2.yml up
```

After the start you can reach the Werserver in the browser with the URL `http://127.0.0.1/`. Some browsers may cache the result, so you should disable the cache in your browser for this test. Example see screenshot below.

![Browser exsample with disabled cache](haproxy_example_browser.png "Browser exsample with disabled cache")

For multiple reloads of the page e.g. with F5, on most browsers, we can see how HAProxy distributes the requests to the three web servers. In this example, the web server provides different content.

The log of the three web servers and HAProxy will be displayed in the console where Docker Compose was started. Since we can see that the requests have been processed by HAProxy and forwarded a web server. The request on the one web server is also visible in the log. Example see screenshot below.

![Docker compose log](haproxy_example_log.png "Docker compose log")

##  3. <a name='Statistics'></a>Statistics

Statistics are turned on in this example and can be accessed at URL `http://127.0.0.1:81/`. To log in, please use `username` as username and the password is `password`.

For example, you can now switch off a web server, this will be displayed as `DOWN` in the statistics and no requests will be forwarded to this web server.

```bash
docker kill $(docker ps -q --filter name=haproxyexample_webserver2_1)
```

![HAProxy statistics report](haproxy_example_stats.png "HAProxy statistics report")

##  4. <a name='Reloadconfiguration'></a>Reload configuration

By default, the configuration file is monitored for changes, and if the checksum is changed, the HAProxy is restarted.

For this you have to open the file `/tmp/haproxy_example/haproxy.cfg` in an editor and make a change, it is enough to add a comment, e.g. `# add empty line for test`.

Within 10 seconds (default value) the change will be detected and the HAProxy will be restarted, this and wet information will also be displayed in the log, see following screenshot.

![HAProxy reload log](haproxy_example_reload.png "HAProxy reload log")

##  5. <a name='Cleanup'></a>Cleanup

Stop Docker compose in the console with `Ctrl + C`.

Delete downloaded files

```bash
rm -rf /tmp/haproxy_example
```

Remove docker containers

```bash
docker ps -a -q --filter name=haproxyexample_ | xargs -n 1 -P 8 -I {} docker stop {} 
docker ps -a -q --filter name=haproxyexample_ | xargs -n 1 -P 8 -I {} docker rm {}
```

If necessary, the used Docker images can be deleted, if they are not used with other containers.

```bash
docker rmi nginx:alpine
docker rmi registry.gitlab.com/epicdocker/haproxy:latest
```
