FROM haproxy:alpine

ARG BUILD_DATE
LABEL org.label-schema.name="HAProxy" \
      org.label-schema.description="HAProxy with stdout logging and automatic configuration reload" \
      org.label-schema.vendor="epicsoft LLC / Alexander Schwarz <as@epicsoft.one>" \
      org.label-schema.version="latest" \
      org.label-schema.schema-version="1.0" \
      org.label-schema.build-date=${BUILD_DATE}

LABEL image.name="epicsoft-haproxy" \
      image.description="HAProxy with stdout logging and automatic configuration reload" \
      maintainer="epicsoft LLC" \
      maintainer.name="Alexander Schwarz <as@epicsoft.one>" \
      maintainer.copyright="Copyright 2018-2023 epicsoft LLC / Alexander Schwarz" \
      license="MIT"

ENV OBSERVE_FILES="/usr/local/etc/haproxy/haproxy.cfg" \
    OBSERVE_FILES_CREATE=true \
    OBSERVE_COMMAND="restartHAProxy" \
    OBSERVE_INTERVAL=10 \
    HAPROXY_CONFIG_FILE="/usr/local/etc/haproxy/haproxy.cfg" \
    HAPROXY_PID_FILE="/var/run/haproxy.stat" \
    HAPROXY_STARTUP_ARGS="-D -p {PID_FILE} -f {CONFIG_FILE}" \
    HAPROXY_VALIDATE_ARGS="-c -V -f {CONFIG_FILE}" \
    CHECK_MK_HTTP_URI="" \
    CHECK_MK_AGENT_NAME="check_mk_agent.linux" \
    CHECK_MK_PLUGIN_NAME="mk_haproxy.freebsd"

USER root

RUN apk --no-cache add bash \
                       socat \
 && rm -rf /var/cache/apk/*

COPY "etc/" "/etc/"

RUN chmod +x /etc/epicsoft/*.sh

ENTRYPOINT [ "/etc/epicsoft/entrypoint.sh" ]

HEALTHCHECK CMD [ "/etc/epicsoft/healthcheck.sh" ]
