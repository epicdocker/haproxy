#!/bin/bash 

ps -a | grep "[h]aproxy" > /dev/null || exit 2

ps -a | grep "[s]yslogd -O /proc/1/fd/1" > /dev/null || exit 3

if [[ "$(echo "$OBSERVE_FILES")" != '' && "$(echo "$OBSERVE_COMMAND")" != '' ]]; then
  ps -a | grep "[b]ash /etc/epicsoft/observer.sh" > /dev/null || exit 4
fi
