#!/bin/bash

#### expected CHECK_MK_HTTP_URI e.g. "https://monitoring.example.com/mysite/check_mk/"

declare -r CHECK_MK_HTTP_AGENTS_PATH="agents/"
declare -r CHECK_MK_HTTP_PLUGINS_PATH="agents/plugins/"

declare -r CHECK_MK_AGENT_TARGET_DIR="/usr/bin"
declare -r CHECK_MK_AGENT_TARGET_NAME="check_mk_agent"

declare -r CHECK_MK_PLUGIN_TARGET_DIR="/usr/lib/check_mk_agent/plugins"
declare -r CHECK_MK_PLUGIN_TARGET_NAME="mk_haproxy.freebsd"

if [[ "$(echo "$CHECK_MK_HTTP_URI")" != '' ]]; then
  echo "[INFO] $(date -Iseconds) Starting installation of Check_MK agent und plugin"
  declare CHECK_MK_HTTP=$CHECK_MK_HTTP_URI
  declare CHECK_MK_HTTP_LENGTH=${#CHECK_MK_HTTP}
  declare CHECK_MK_HTTP_LAST_CHAR=${CHECK_MK_HTTP:CHECK_MK_HTTP_LENGTH-1:1}
  declare CHECK_MK_AGENT_HTTP_DOWNLOAD_URI
  declare CHECK_MK_PLUGIN_HTTP_DOWNLOAD_URI

  #### Add trailing slash if needed
  if [[ $CHECK_MK_HTTP_LAST_CHAR != "/" ]]; then 
    CHECK_MK_HTTP="$CHECK_MK_HTTP/"
  fi

  echo "[INFO] $(date -Iseconds) Downloading Check_MK agent: $CHECK_MK_AGENT_NAME"
  CHECK_MK_AGENT_HTTP_DOWNLOAD_URI="$CHECK_MK_HTTP$CHECK_MK_HTTP_AGENTS_PATH$CHECK_MK_AGENT_NAME"
  echo "[DEBUG] $(date -Iseconds) Agent download URI: $CHECK_MK_AGENT_HTTP_DOWNLOAD_URI"
  wget $CHECK_MK_AGENT_HTTP_DOWNLOAD_URI -O "$CHECK_MK_AGENT_TARGET_DIR/$CHECK_MK_AGENT_TARGET_NAME"
  chmod +x "$CHECK_MK_AGENT_TARGET_DIR/$CHECK_MK_AGENT_TARGET_NAME"
  
  echo "[INFO] $(date -Iseconds) Downloading Check_MK plugin: $CHECK_MK_PLUGIN_NAME"
  echo "[DEBUG] $(date -Iseconds) Create plugin directory: $CHECK_MK_PLUGIN_TARGET_DIR"
  mkdir -p $CHECK_MK_PLUGIN_TARGET_DIR

  CHECK_MK_PLUGIN_HTTP_DOWNLOAD_URI="$CHECK_MK_HTTP$CHECK_MK_HTTP_PLUGINS_PATH$CHECK_MK_PLUGIN_NAME"
  echo "[DEBUG] $(date -Iseconds) Plugin download URI: $CHECK_MK_PLUGIN_HTTP_DOWNLOAD_URI"
  wget $CHECK_MK_PLUGIN_HTTP_DOWNLOAD_URI -O "$CHECK_MK_PLUGIN_TARGET_DIR/$CHECK_MK_PLUGIN_TARGET_NAME"
  chmod +x "$CHECK_MK_PLUGIN_TARGET_DIR/$CHECK_MK_PLUGIN_TARGET_NAME"

  echo "[INFO] $(date -Iseconds) Check_MK agent und plugin installed"
fi
