#!/bin/bash 

function startHAProxy(){
  local COMMAND=$(echo "haproxy $HAPROXY_STARTUP_ARGS" | sed -e "s#{PID_FILE}#$HAPROXY_PID_FILE#g" | sed -e "s#{CONFIG_FILE}#$HAPROXY_CONFIG_FILE#g")
  echo "[INFO] $(date -Iseconds) Starting HAProxy - '$COMMAND'"
  $COMMAND
}

function stopHAProxy(){
  echo "[INFO] $(date -Iseconds) Stopping HAProxy"
  for PID in $(cat $HAPROXY_PID_FILE) ; do
    kill $PID
  done
  rm -f $HAPROXY_PID_FILE
}

function validateHAProxyConfiguration(){
  local COMMAND=$(echo "haproxy $HAPROXY_VALIDATE_ARGS" | sed -e "s#{CONFIG_FILE}#$HAPROXY_CONFIG_FILE#g")
  echo "[INFO] $(date -Iseconds) Validate HAProxy configuration - '$COMMAND'"
  $COMMAND
}

function restartHAProxy(){
  if validateHAProxyConfiguration; then   
    stopHAProxy
    startHAProxy
  else
    echo "[WARNING] $(date -Iseconds) Configuration faulty, HAProxy will NOT be restarted"
  fi
}
