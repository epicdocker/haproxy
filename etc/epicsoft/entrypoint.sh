#!/bin/bash

set -e

#### HAProxy show current version
haproxy -v

#### Call entrypoint from the base image
sh /usr/local/bin/docker-entrypoint.sh

#### Starting syslog for stdout
syslogd -O /proc/1/fd/1

#### Start Check_MK installation
bash /etc/epicsoft/check_mk.sh

#### Start observer as a background process
bash /etc/epicsoft/observer.sh &

#### Starting HAProxy
source /etc/epicsoft/haproxy.functions.sh
startHAProxy

#### Execute command or keep container alive
exec "$@" && tail -f /dev/null
