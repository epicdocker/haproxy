# HAProxy

<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
* 2. [Requirements](#Requirements)
* 3. [Configuration](#Configuration)
* 4. [Examples](#Examples)
* 5. [Environments](#Environments)
* 6. [Check_MK activation](#Check_MKactivation)
* 7. [Links](#Links)
* 8. [License](#License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

HAProxy with stdout logging and automatic configuration reload.

This Docker Image uses official [HAProxy](https://hub.docker.com/_/haproxy/) image and extends it with some functionalities.
- Logging to the console is enabled with `syslogd`, which will use Docker Container's default logging behaviors.
- Changes to the configuration are applied without restarting the container or service.

The information about changes to files is not passed on to the container by the host, therefore checksums are created by the files and checked for changes in the interval. In case of a faulty HAProxy configuration, the application and the bash-script will continue, so that the error can be corrected without container restart.

##  1. <a name='Versions'></a>Versions

There is no versioning of this project, the current version of `alpine` (`latest`) is created weekly to keep the image up to date.

If you need a different version, you may copy and modify my templates as needed.

`alpine` `latest` [Dockerfile](https://gitlab.com/epicdocker/haproxy/blob/master/Dockerfile)

##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-started
- Optional: Docker Compose installed (is needed for examples) - https://docs.docker.com/compose/

##  3. <a name='Configuration'></a>Configuration

To pass the HAProxy logs to the Docker container so that they are output in the STDOUT, the configuration must be extended as indicated. Log level can be adjusted as needed and more loggers with different names can be added.

```
global 
  #### Log levels: emerg, alert, crit, err, warning, notice, info, debug
  log   /dev/log local0 notice

defaults
  log   global
```

##  4. <a name='Examples'></a>Examples

See example [haproxy_example_v2.md](example/haproxy_example_v2.md)

##  5. <a name='Environments'></a>Environments

`OBSERVE_FILES`

_default:_ /usr/local/etc/haproxy/haproxy.cfg

A list of files with absolute paths to monitor for change (based on the checksum). Both one and several files may be specified, these must be separated by a space.

`OBSERVE_FILES_CREATE` 

_default:_ true

Checks the list of files and creates files without content that does not exist.

`OBSERVE_COMMAND` 

_default:_ restartHAProxy

The command to execute, if any of the submitted files changes. `restartHAProxy` is a predefined function, which can be replaced by any bash command.

`OBSERVE_INTERVAL`

_default:_ 10

Interval in seconds, in which the changes are checked.

`HAPROXY_CONFIG_FILE`

_default:_ /usr/local/etc/haproxy/haproxy.cfg

Absolute path to the HAProxy configuration file within the Docker container. Is passed as parameter when the application is started.

`HAPROXY_PID_FILE`

_default:_ /var/run/haproxy.stat

Absolute path to the HAProxy PID file inside the Docker container. Is passed as parameter when the application is started.

`HAPROXY_STARTUP_ARGS`

_default:_ -D -p {PID_FILE} -f {CONFIG_FILE}

Starting parameters of HAProxy, in normal case these should not be changed. The placeholder `{PID_FILE}` is replaced with the value from the environment `HAPROXY_PID_FILE`. The placeholder `{CONFIG_FILE}` is replaced with the value from the environment `HAPROXY_CONFIG_FILE`.

`HAPROXY_VALIDATE_ARGS`

_default:_ -c -V -f {CONFIG_FILE}

Start parameter of a validation of the configuration file without starting the HAProxy. The placeholder `{CONFIG_FILE}` is replaced with the value from the environment `HAPROXY_CONFIG_FILE`.

`CHECK_MK_HTTP_URI`

_default:_ _empty_

URI to the Check_MK page e.g `https://monitoring.example.com/mysite/check_mk/`, this must be reachable from the container. If a valid URI is specified, the installation of the Check_MK agent and plugin will be done at startup from the container.

`CHECK_MK_AGENT_NAME`

_default:_ check_mk_agent.linux

The Check_MK agent to install. Installation is only performed if `CHECK_MK_HTTP_URI` is specified.

`CHECK_MK_PLUGIN_NAME`

_default:_ mk_haproxy.freebsd

The Check_MK plugin to install. Installation is only performed if `CHECK_MK_HTTP_URI` is specified.

##  6. <a name='Check_MKactivation'></a>Check_MK activation

If you already have Check_MK and the plugin `mk_docker_container_piggybacked` in use, you can use this extension to read out and monitor the HAProxy status from the container.

In order for the Check_MK agent and plugin to be downloaded at startup, it is recommended to specify a valid and container accessible URI e.g. `https://monitoring.example.com/mysite/check_mk/`. The URI must be set in environment `CHECK_MK_HTTP_URI`.

Example:

```bash
docker run --name myhaproxy \
  -e CHECK_MK_HTTP_URI=https://monitoring.example.com/mysite/check_mk/ \
  -v /srv/haproxy/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg:ro \
  registry.gitlab.com/epicdocker/haproxy:alpine
```

The configuration of the HAProxy must be extended by `stats socket` to provide a socket.

Example: 

```
global 
  #### Log levels: emerg, alert, crit, err, warning, notice, info, debug
  log   /dev/log local0 notice
  stats socket /var/run/haproxy.sock mode 440 level admin
  [...]
```

Please note:
- HAProxy PID file must be set to `/var/run/haproxy.stat` _(default)_
- Socket name must be set to `/var/run/haproxy.sock` in the HAProxy configuration
- Environment `CHECK_MK_HTTP_URI` must contain correct URI

##  7. <a name='Links'></a>Links

- https://gitlab.com/epicdocker/haproxy
- https://hub.docker.com/r/epicsoft/haproxy/
- https://quay.io/repository/epicsoft/haproxy

##  8. <a name='License'></a>License

MIT License see [LICENSE](https://gitlab.com/epicdocker/haproxy/blob/master/LICENSE)
